import OSC from "osc-js";
export default {
    Backend:{ 
        port:8080
    },
    customization:{
        logo:{
            file:"logo.png",
            width:"211px"
        },
        background_color:"#151515",
        accent_color:"#fff"
    },
    "osc":{
        "connection":{
            plugin: new OSC.BridgePlugin({
                udpServer:{
                    host: '0.0.0.0',
                    port:9020,
                    exclusive:false
                },
                udpClient:{
                    host:'127.0.0.1',
                    port:9004
                },
                receiver:'udp'
            })
        },
        "commands":{
            "clip":{
                "address":"/mov/playMovie"
            },
            "play":{
                "address":"/mov/play"
            },
            "pause":{
                "address":"/mov/pause"
            }
        }
    }
}