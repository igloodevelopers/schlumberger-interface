import assets from "./conf/assets.json";
import conf from "./conf/main";
import Iglosc from "./util/osc";
import Server from "./util/webserver-backend";

const osc = new Iglosc(conf.osc.connection,conf.osc.commands);
const server = new Server(conf,assets);

server.on("playVideo",()=>osc.play());
server.on("pauseVideo",()=>osc.pause());
server.on("triggerMedia",(media)=>osc.clip(media));
server.on("sendOSC",(command,args)=>osc.send(command,args));

server.on("error",(error)=>console.log(`Server Error: ${error}`));

osc.on("update",update=>server.update(update));
osc.on("verbose",message=>console.log(message));

osc.listen();
server.open();