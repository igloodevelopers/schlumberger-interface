import Express from "express";
import http from 'http';
import path from "path";
import { EventEmitter } from "events";

export default class Backend extends EventEmitter{
    constructor(conf, assetStore){
        super();
        this.assetStore = assetStore;
        this.conf = conf;
        this.contentList={categories:[]};
        this.app = new Express();
        this.router = new Express.Router();
        this.status = {
            playing:false,
            clip:""
        }

        //sends startup information to client
        this.router.route("/connectClient").get((req,res)=>{
            this.startClientConnection(req,res);
        });
        //Playback controls ===============
        this.router.route('/play').get((req,res)=> {
            this.emit("playVideo");
            this.status.playing=true;
            res.end("success");
        });
        this.router.route('/pause').get((req,res)=>{
            this.emit("pauseVideo");
            this.status.playing = false;
            res.end("success");
        });
        this.router.route("/triggerMedia/:media").get((req,res)=>{
            this.emit("triggerMedia",req.params.media);
            this.status.clip = req.params.media;
            res.end("success");
        });
        this.router.route("/status").get((req,res)=>{

        });
        this.app.use("/api",this.router);
        this.app.use("/",Express.static(path.join(__dirname,'html')));
    }

    //returns all of the assets
    getFullAssetList = () => {
        return this.assetStore.assets;
    }

    //returns all of the assets that belong to a certain tag
    getAssetsFromTag = (tag) => {
        var assetList = [];
        this.assetStore.assets.forEach(asset => {
            if(asset.tags.includes(tag)){
                assetList.push(asset);
            }
        });
        return assetList;
    }

    //returns all of the categories
    getCategories = () => {
        return this.assetStore.categories;
    }

    //sends an osc message
    sendOSC(command,args){
        this.emit("SendOSC",command,args);
    }

    //starts the module, and sorts out the asset store
    open = () => {
        const self=this;
        //sort out all of the categories and assets
        this.getCategories().forEach(category=>{
            var cat = {"name":category.name,"assets":{}};
            category.tags.forEach(tag=>{
                var tagPretty = self.assetStore.tags[tag];
                cat.assets[tagPretty] = self.getAssetsFromTag(tag);
            });
            self.contentList.categories.push(cat);
        }); 
        console.log(this.contentList);
        http.createServer(this.app).listen(this.conf.Backend.port);
        console.log(this.conf.Backend.port);
        return this.conf.Backend.port
    }

    //send connection data to web client
    startClientConnection = (req,res) =>{
        //fixing js
        const self=this;
        //prepare bundle
        var clientdetails = {
            "customization":self.conf.customization,
            "categories":self.contentList.categories
        }
        console.log("client connected");
        res.json(clientdetails);
        return clientdetails;
    }
    
}