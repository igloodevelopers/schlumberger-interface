import { EventEmitter } from "events";
import OSC from "osc-js"

export default class Iglosc extends EventEmitter{
    constructor(port,commands){
        super();
        this.port = port;
        this.commands = commands;
        this.osc = new OSC(port);
    }
    play(){
		//this.sendOSC(this.commands.play.address,null);
        this.sendOSC(this.commands.pause.address,0);
    }

    pause(){
		//this.sendOSC(this.commands.pause.address,null);
        this.sendOSC(this.commands.pause.address,1);
    }
    clip(filename){
        this.sendOSC(this.commands.clip.address,filename)
    }
    sendOSC(command,messages){
        if (messages == null){
            messages = 1;
        }
        this.emit("verbose", `Sending OSC: '${command}' ${messages}`);
        this.osc.send(new OSC.Message(command,messages));
    }
    listen(){

    }
}