var state = {
    playing:false,
    clipStarted:false
};

//setup
$(document).ready(()=>{
    console.log("loaded document");
    $.getJSON("/api/connectClient",(data)=>{
        console.log(data);
        state=data;
        loadContent(data.categories);
        loadCustomization(data.customization);
        setupOnClick();
        setupPage();
    });
});

window.onscroll = () => {
    var catbar = $("#categories");
    if(window.pageYOffset>500){
        catbar.addClass("stuck");
        $("#stuckLogo").removeClass("hidden");
    } else {
        catbar.removeClass("stuck");
        $("#stuckLogo").addClass("hidden");
    }
}

function loadContent(categories){
    $("#categories").html("");
    $(".contentBox").remove();
    var i=0;
    categories.forEach(category => {
        $("#categories").append(`<div class="category" data-content-box="#category-box-${i}">${category.name}</div>`);

        $("body").append(`<div class="contentBox hidden" id="category-box-${i}"></div>`);
        Object.keys(category.assets).forEach(tag=>{
            console.log(tag)
            $(`#category-box-${i}`).append(`<div class="header">${tag}</div>`);
            category.assets[tag].forEach(asset=>{
                $(`#category-box-${i}`).append(`<div class="contentItem" data-filename="${asset.filename}" data-preview-name="${asset.preview}"><img src="assets/thumbnails/${asset.thumbnail}"/><div class=darken><img src="img/smallPlayButton.svg" class="playbutton"></div></div>`);
            })
        })
        i++;
    });
}

function loadCustomization(customization){

}

function setupOnClick(){
    $(".category").click((target)=>{
        console.log("click");
        console.log(target.currentTarget);
        //reset all
        $(".contentBox").removeClass("hidden").addClass("hidden");
        //remove
        $($(target.currentTarget).data("content-box")).removeClass("hidden");
        $(".category").removeClass("selected");
        $($(target.currentTarget)).addClass("selected");
    });
    //click on content
    $(".contentItem").click((target)=>{
        console.log(`clicked content: ${target}`);
        //set play button data
        $("#bigPlayButton").data("clip",$(target.currentTarget).data("filename"));
        //unhide play buttons
        $(".contentItem").removeClass("selected");
        $($(target.currentTarget)).addClass("selected");

        //change preview
        $(".scrollingimg").fadeOut(400,()=>{
            $(".scrollingimg").css(`background-image`,`url("../assets/${$(target.currentTarget).data("preview-name")}")`);
            $(".scrollingimg").fadeIn();
        });
        
        console.log(`url(../assets/${$(target.currentTarget).data("preview-name")});`);
    });

    //click on play button, send command
    $("#bigPlayButton").click((target)=>{
        if($(target.currentTarget).data("clip")==null){
            return;
        }
        sendCommand("play",$(target.currentTarget).data("clip"));
    });

    $("#resumeButton").click(()=>{
        sendCommand("resume",null)
    });
    $("#pauseButton").click(()=>{
        sendCommand("pause",null)
    });
}
function sendCommand(command,data){
    var request = "";
    switch (command) {
        case "play":
            if(data==null){
                console.log("no data");
                return;
            }
            request = `triggerMedia/${data}`;
            break;

        case "pause":
            request = `pause`;
            break;
        
        case "resume":
            request = `play`;
            break;
    
        default:
            break;
    }
    $.ajax({
        url: `/api/${request}`
    }).done(function (resp) {
        console.log(resp);
    });
}
